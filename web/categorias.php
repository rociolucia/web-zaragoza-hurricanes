<!-- head -->
<?php include("includes/head.php") ?>
<!-- //head -->

<body>

<!-- menu -->
<?php
include "includes/menu.php";
	
	include "Conexion/conexion.php";
?>
<!-- //menu -->

	<div class="inner-page-banner text-center">
		<div class="banner-dott3">
			<div class="container">
				<br><br><br><br>
				<h2 class="text-capitalize"></h2>
				<strong>
					<p><a href="index.php"></a></p>
				</strong>
			</div>
		</div>
	</div>
	<!-- //logo + menu -->
	<!--// header -->

	<!-- Grupo Categorias-->
	<section class="blog py-5 my-lg-5 my-3">

		<div class="text-center icon titulos-home4"> <span><i class="fas fa-football-ball"></i></span> </div>
		<h3 class="heading text-center text-capitalize mb-5 titulos-home3"> Nuestras Categorías</h3>
		<div class="container-fluid">
			<div class="row blog-posts">


				<div class="col-lg-4 col-sm-8 blog1 p-sm-5 p-4">
					<h4 class="texto-azul">Senior</h4>
					<h3 class="py-4">Con un crecimiento vertiginoso desde 2018 tras ganar varios campeonatos en la Liga
						Norte.</h3>

					<a href="#senior">LEER MÁS </a>
				</div>
				<div class="col-lg-2 col-sm-4 blogimg p-0">

				</div>

				<div class="col-lg-4 col-sm-8 blog2 p-sm-5 p-4">
					<h4>Femenino</h4>
					<h3 class="py-4">Somos un equipo femenino de football americano formado por 16 chicas y 3
						componentes del Staff.</h3>

					<a href="#femenino">LEER MÁS </a>
				</div>
				<div class="col-lg-2 col-sm-4 blogimg3 p-0">

				</div>


				<div class="col-lg-4 col-sm-8 blog2 p-sm-5 p-4">
					<h4 class="text-azul">Junior</h4>
					<h3 class="py-4">El equipo está compuesto por jóvenes de 15 a 18 años. Actualmente competimos en la
						primera división nacional de FEFA</h3>

					<a href="#junior">LEER MÁS </a>
				</div>
				<div class="col-lg-2 col-sm-4 blogimg1 p-0">

				</div>
				<div class="col-lg-2 col-sm-4 blogimg2 p-0">

				</div>



				<div class="col-lg-4 col-sm-8 blog1 blog3 p-sm-5 p-4">
					<h4>Academy</h4>
					<h3 class="py-4">Equipo formado por menores de 14 años en una modalidad del football sin contacto
						físico.

					</h3>

					<a href="#academy">LEER MÁS </a>

				</div>
			</div>
		</div>
	</section>
	<!-- //Grupo Categorias-->

	<!-- seccion SENIOR -->
	<section class="welcome py-5 my-lg-5 my-3 titulos-home5" id="senior">
		<center>
			<h2 class="heading-agileinfo titulos-home" id="centrado">Equipo <span> Senior</span></h2>
		</center>
		<div class="container">
			<div class="row mt-5 about-bottom">
				<div class="col-lg-6 welcome-left">
					<img src="images/a1.jpg" alt="" class="img-fluid" />
				</div>
				<div class="col-lg-6 mt-lg-0 mt-5 welcome-right">

					<p class="welcome-para mb-3 text-left"><br>Con un crecimiento vertiginoso desde 2018 tras ganar
						varios
						campeonatos en la Liga Norte la plantilla del corazón del equipo ya se compone por más de 50
						jugadores de la ciudad. <br><br>Además, el club invierte en varios “import” de diferentes países
						cada
						temporada para jugar en el Senior masculino, enseñarnos y ayudarnos en todas las demás
						categorías del club.<br><br> Nuestros gladiadores son los verdaderos culpables de haber devuelto
						al
						equipo a la máxima categoría nacional hace dos años.</p>

					<h6 class="w3l-style titulos-home9" id="centrado2 textdesp">SENIOR</h6>

				</div>
			</div>
		</div>
		</div>
	</section>
	<!-- //seccion SENIOR -->

	<!-- SENIOR imagenes -->
	<center>
		<section class="about py-sm-5 mt-lg-5 mt-sm-3 pt-0 pb-5" id="about">
			<div class="w3agile-spldishes">
				<div class="container">
					<div class="spldishes-agileinfo">
						<div class="spldishes-grids">
							<div id="owl1-demo" class="owl-carousel agileinfo-models-row titulos-home4">
								<div class="item g1">
									<img class="lazyOwl" src="images/a10.jpg" title="Fashion Slider" alt="" />

								</div>
								<div class="item g1">
									<img class="lazyOwl" src="images/a2.jpg" title="Fashion Slider" alt="" />

								</div>
								<div class="item g1">
									<img class="lazyOwl" src="images/a133.jpg" title="Fashion Slider" alt="" />

								</div>
								<div class="item g1">
									<img class="lazyOwl" src="images/a134.jpg" title="Fashion Slider" alt="" />

								</div>
								<div class="item g1">
									<img class="lazyOwl" src="images/a135.jpg" title="Fashion Slider" alt="" />

								</div>
								<div class="item g1">
									<img class="lazyOwl" src="images/a132.jpg" title="Fashion Slider" alt="" />

								</div>
								<div class="item g1">
									<img class="lazyOwl" src="images/a131.jpg" title="Fashion Slider" alt="" />

								</div>
								<div class="item g1">
									<img class="lazyOwl" src="images/a4.jpg" title="Fashion Slider" alt="" />

								</div>
								<div class="item g1">
									<img class="lazyOwl" src="images/a3.jpg" title="Fashion Slider" alt="" />

								</div>
								
								<div class="item g1">
									<img class="lazyOwl" src="images/a5.jpg" title="Fashion Slider" alt="" />

								</div>
								<div class="item g1">
									<img class="lazyOwl" src="images/a6.jpg" title="Fashion Slider" alt="" />

								</div>
								<div class="item g1">
									<img class="lazyOwl" src="images/a7.jpg" title="Fashion Slider" alt="" />

								</div>
								<div class="item g1">
									<img class="lazyOwl" src="images/a8.jpg" title="Fashion Slider" alt="" />

								</div>
								<div class="item g1">
									<img class="lazyOwl" src="images/a9.jpg" title="Fashion Slider" alt="" />

								</div>
								<div class="item g1">
									<img class="lazyOwl" src="images/a100.jpg" title="Fashion Slider" alt="" />

								</div>
								<div class="item g1">
									<img class="lazyOwl" src="images/a101.jpg" title="Fashion Slider" alt="" />

								</div>
								<div class="item g1">
									<img class="lazyOwl" src="images/a102.jpg" title="Fashion Slider" alt="" />

								</div>
								<div class="item g1">
									<img class="lazyOwl" src="images/a103.jpg" title="Fashion Slider" alt="" />

								</div>
								<div class="item g1">
									<img class="lazyOwl" src="images/a104.jpg" title="Fashion Slider" alt="" />

								</div>
								<div class="item g1">
									<img class="lazyOwl" src="images/a105.jpg" title="Fashion Slider" alt="" />

								</div>
								<div class="item g1">
									<img class="lazyOwl" src="images/a106.jpg" title="Fashion Slider" alt="" />

								</div>
								<div class="item g1">
									<img class="lazyOwl" src="images/a107.jpg" title="Fashion Slider" alt="" />

								</div>
								<div class="item g1">
									<img class="lazyOwl" src="images/a108.jpg" title="Fashion Slider" alt="" />

								</div>
								<div class="item g1">
									<img class="lazyOwl" src="images/a109.jpg" title="Fashion Slider" alt="" />

								</div>
								<div class="item g1">
									<img class="lazyOwl" src="images/a110.jpg" title="Fashion Slider" alt="" />

								</div>
								<div class="item g1">
									<img class="lazyOwl" src="images/a111.jpg" title="Fashion Slider" alt="" />

								</div>
								<div class="item g1">
									<img class="lazyOwl" src="images/a112.jpg" title="Fashion Slider" alt="" />

								</div>
								<div class="item g1">
									<img class="lazyOwl" src="images/a113.jpg" title="Fashion Slider" alt="" />

								</div>
								<div class="item g1">
									<img class="lazyOwl" src="images/a114.jpg" title="Fashion Slider" alt="" />

								</div>
								<div class="item g1">
									<img class="lazyOwl" src="images/a115.jpg" title="Fashion Slider" alt="" />

								</div>
								<div class="item g1">
									<img class="lazyOwl" src="images/a116.jpg" title="Fashion Slider" alt="" />

								</div>
								<div class="item g1">
									<img class="lazyOwl" src="images/a117.jpg" title="Fashion Slider" alt="" />

								</div>
								<div class="item g1">
									<img class="lazyOwl" src="images/a118.jpg" title="Fashion Slider" alt="" />

								</div>
								<div class="item g1">
									<img class="lazyOwl" src="images/a119.jpg" title="Fashion Slider" alt="" />

								</div>
								<div class="item g1">
									<img class="lazyOwl" src="images/a120.jpg" title="Fashion Slider" alt="" />

								</div>
								<div class="item g1">
									<img class="lazyOwl" src="images/a121.jpg" title="Fashion Slider" alt="" />

								</div>
								<div class="item g1">
									<img class="lazyOwl" src="images/a122.jpg" title="Fashion Slider" alt="" />

								</div>
								<div class="item g1">
									<img class="lazyOwl" src="images/a123.jpg" title="Fashion Slider" alt="" />

								</div>
								<div class="item g1">
									<img class="lazyOwl" src="images/a124.jpg" title="Fashion Slider" alt="" />

								</div>
								<div class="item g1">
									<img class="lazyOwl" src="images/a125.jpg" title="Fashion Slider" alt="" />

								</div>
								<div class="item g1">
									<img class="lazyOwl" src="images/a126.jpg" title="Fashion Slider" alt="" />

								</div>
								<div class="item g1">
									<img class="lazyOwl" src="images/a127.jpg" title="Fashion Slider" alt="" />

								</div>
								<div class="item g1">
									<img class="lazyOwl" src="images/a128.jpg" title="Fashion Slider" alt="" />

								</div>
								<div class="item g1">
									<img class="lazyOwl" src="images/a129.jpg" title="Fashion Slider" alt="" />

								</div>
								<div class="item g1">
									<img class="lazyOwl" src="images/a130.jpg" title="Fashion Slider" alt="" />

								</div>
								
								
							</div>
						</div>
						
					</div>
				</div>
			</div>
			
		</section>
		<a class="boton_1" href="partidos_senior.php">Partidos disputados</a>
	</center>
	<!-- //SENIOR imagenes -->
	<!-- seccion FEMENINO-->
	<section class="welcome py-5 my-lg-5 my-3 titulos-home5" id="femenino">
		<center>
			<h2 class="heading-agileinfo titulos-home" id="centrado">Equipo <span> Femenino</span></h2>
		</center>
		<div class="container">
			<div class="row mt-5 about-bottom">
				<div class="col-lg-6 welcome-left">
					<img src="images/a21.jpg" alt="" class="img-fluid" />
				</div>
				<div class="col-lg-6 mt-lg-0 mt-5 welcome-right">

					<p class="welcome-para mb-3 text-left">Equipo formado por 16
						chicas y 3 componentes del Staff. Lo más importante para nosotras es establecer unas bases
						sólidas y enriquecidas de
						chicas que quieran llevar al equipo femenino a ser un equipo consolidado en España. Lo siguiente
						es formar y nutrir de principios tan hermosos como la disciplina, el compañerismo y la lealtad
						entre compañeras. Después de lograr esos objetivos conseguir un hueco en la historia del fútbol
						femenino
						en España. En el Club Hurricanes Zaragoza no hay límites ni diferencias entre sexos, solo
						unidad,
						compromiso y mucha ilusión. Así que si quieres probar un deporte de unidad y compañerismo,
						conocer lugares y personas increíbles te invito con nosotras. ¡Los límites los pones tú!</p>

					<h6 class="w3l-style titulos-home8" id="centrado2 textdesp">FEMENINO</h6>

				</div>
			</div>
		</div>
		</div>
	</section>
	<!-- //seccion FEMENINO-->

	<!-- FEMENINO imagenes -->
	<center>
		<section class="about py-sm-5 mt-lg-5 mt-sm-3 pt-0 pb-5" id="about">
			<div class="w3agile-spldishes">
				<div class="container">
					<div class="spldishes-agileinfo">
						<div class="spldishes-grids">
							<div id="owl3-demo" class="owl-carousel agileinfo-models-row titulos-home4">
								<div class="item g1">
									<img class="lazyOwl" src="images/a22.jpg" title="Fashion Slider" alt="" />

								</div>
								
								<div class="item g1">
									<img class="lazyOwl" src="images/a23.jpg" title="Fashion Slider" alt="" />

								</div>
								<div class="item g1">
									<img class="lazyOwl" src="images/a24.jpg" title="Fashion Slider" alt="" />

								</div>
								<div class="item g1">
									<img class="lazyOwl" src="images/a25.jpg" title="Fashion Slider" alt="" />

								</div>
								<div class="item g1">
									<img class="lazyOwl" src="images/a26.jpg" title="Fashion Slider" alt="" />

								</div>
								<div class="item g1">
									<img class="lazyOwl" src="images/a27.jpg" title="Fashion Slider" alt="" />

								</div>
								<div class="item g1">
									<img class="lazyOwl" src="images/a28.jpg" title="Fashion Slider" alt="" />

								</div>
								<div class="item g1">
									<img class="lazyOwl" src="images/a29.jpg" title="Fashion Slider" alt="" />

								</div>
								<div class="item g1">
									<img class="lazyOwl" src="images/a30.jpg" title="Fashion Slider" alt="" />

								</div>
								<div class="item g1">
									<img class="lazyOwl" src="images/a31.jpg" title="Fashion Slider" alt="" />

								</div>
								<div class="item g1">
									<img class="lazyOwl" src="images/a32.jpg" title="Fashion Slider" alt="" />

								</div>
								<div class="item g1">
									<img class="lazyOwl" src="images/a33.jpg" title="Fashion Slider" alt="" />

								</div>
								<div class="item g1">
									<img class="lazyOwl" src="images/a34.jpg" title="Fashion Slider" alt="" />

								</div>
							
							</div>
						</div>
						<div class="clearfix"> </div>
					</div>
				</div>
			</div>
			<br><a class="boton_1" href="partidos_femenino.php">Partidos disputados</a>
		</section>
	</center>
	<!-- //FEMENINO imagenes -->


	<!-- seccion JUNIOR -->
	<section class="welcome py-5 my-lg-5 my-3 titulos-home5" id="junior">
		<center>
			<h2 class="heading-agileinfo titulos-home" id="centrado">Equipo <span> Junior</span></h2>
		</center>
		<div class="container">
			<div class="row mt-5 about-bottom">
				<div class="col-lg-6 welcome-left">
					<img src="images/a11.jpg" alt="" class="img-fluid" />
				</div>
				<div class="col-lg-6 mt-lg-0 mt-5 welcome-right">

					<p class="welcome-para mb-3 text-left">El equipo JUNIOR de Zaragoza Hurricanes está compuesto por
						jóvenes de 15 a 18 años. Actualmente el equipo compite en la primera división nacional de FEFA
						en su categoría de Junior, pudiendo así viajar por el territorio nacional con el autobús del
						Club y disfrutar así de experiencias inolvidables.

						En el 2012 fuimos campeones de España en la categoría cadete y subcampeones en la categoría
						junior. Compuesto ya por más de 20 jugadores de la ciudad, son la "vida" y "la alegría" del club
						y el número de jugadores está creciendo de año en año, siendo la cuna y la garantía del primer
						equipo masculino.


						Valores, sentimiento de pertenencia, experiencias inolvidables... porque en esta disciplina
						todos los que jóvenes tienen cabida, ¡atrévete a probar!</p>

					<center><h6 class="w3l-style titulos-home7" id="centrado2 textdesp">JUNIOR</h6></center>

				</div>
			</div>
		</div>
		</div>
	</section>
	<!-- //seccion JUNIOR -->
	<!-- JUNIOR imagenes -->
	<center>
		<section class="about py-sm-5 mt-lg-5 mt-sm-3 pt-0 pb-5" id="about">
			<div class="w3agile-spldishes">
				<div class="container">
					<div class="spldishes-agileinfo">
						<div class="spldishes-grids">
							<div id="owl2-demo" class="owl-carousel agileinfo-models-row titulos-home4">
								<div class="item g1">
									<img class="lazyOwl" src="images/a12.jpg" title="Fashion Slider" alt="" />

								</div>
								<div class="item g1">
									<img class="lazyOwl" src="images/a13.jpg" title="Fashion Slider" alt="" />

								</div>
								<div class="item g1">
									<img class="lazyOwl" src="images/a14.jpg" title="Fashion Slider" alt="" />

								</div>
								<div class="item g1">
									<img class="lazyOwl" src="images/a15.jpg" title="Fashion Slider" alt="" />

								</div>
								<div class="item g1">
									<img class="lazyOwl" src="images/a16.jpg" title="Fashion Slider" alt="" />

								</div>
								<div class="item g1">
									<img class="lazyOwl" src="images/a17.jpg" title="Fashion Slider" alt="" />

								</div>
								<div class="item g1">
									<img class="lazyOwl" src="images/a18.jpg" title="Fashion Slider" alt="" />

								</div>
								<div class="item g1">
									<img class="lazyOwl" src="images/a19.jpg" title="Fashion Slider" alt="" />

								</div>
								<div class="item g1">
									<img class="lazyOwl" src="images/a20.jpg" title="Fashion Slider" alt="" />

								</div>
							</div>
						</div>
						<div class="clearfix"> </div>
					</div>
				</div>
			</div>
			<br><a class="boton_1" href="partidos_junior.php">Partidos disputados</a>
		</section>
	</center>
	<!-- //JUNIOR imagenes -->

	<!-- seccion ACADEMY -->
	<section class="welcome py-5 my-lg-5 my-3 titulos-home5" id="academy">
		<center>
			<h2 class="heading-agileinfo titulos-home" id="centrado">Equipo <span> Academy</span></h2>
		</center>
		<div class="container">
			<div class="row mt-5 about-bottom">
				<div class="col-lg-6 welcome-left">
					<img src="images/a37.png" alt="" class="img-fluid" />
				</div>
				<div class="col-lg-6 mt-lg-0 mt-5 welcome-right">

					<p class="welcome-para mb-3 text-left">El gran proyecto que estamos ofreciendo como club a la
						ciudad de Zaragoza es la Hurricanes-Academy. Es una apuesta para consolidar el deporte entre los
						más jóvenes de Zaragoza y que puedan aplicar los valores deportivos dentro y fuera del campo
						desde temprana edad.
						Jóvenes de menos de 14 años pueden disfrutar del fútbol americano sin contacto (flag-football)
						siempre tutelados por los jugadores y entrenadores más veteranos del Club y con el valor añadido
						de que todos los entrenamientos se hacen en inglés con jugadores estadounidenses que pone el
						Club a su disposición.</p>

					<h6 class="w3l-style titulos-home6" id="centrado2 textdesp">ACADEMY</h6>

				</div>
			</div>
		</div>
		</div>
	</section>
	<!-- //seccion HURRICANES ACADEMY -->

	<!-- footer -->
	<?php
include "includes/footer.php"
?>
<!-- //footer -->


	<!-- js-scripts -->

	<!-- js -->
	<script type="text/javascript" src="js/jquery-2.2.3.min.js"></script>
	<script type="text/javascript" src="js/bootstrap.js"></script>
	<script type="text/javascript" src="js/numscroller-1.0.js"></script>
	<script type="text/javascript" src="assets3/js/all.js"></script>
	<script type="text/javascript" src="assets3/js/app.js"></script>
	<script type="text/javascript" src="assets3/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="assets3/js/imagezoom.js"></script>
	<script type="text/javascript" src="assets3/js/jquery.flexslider.js"></script>
	<script type="text/javascript" src="assets3/js/jquery.magnific-popup.js"></script>
	<script type="text/javascript" src="assets3/js/jquery.prettyPhoto.js"></script>
	<script type="text/javascript" src="assets3/jquery.quicksand.js"></script>
	<script type="text/javascript" src="assets3/jquery-2.1.4.min.js"></script>
	<!-- Necessary-JavaScript-File-For-Bootstrap -->
	<!-- //js -->

	<!-- navigation  -->
	<script src="js/main.js"></script>
	<!-- //navigation -->
	<!-- /numscroller -->
	<script type="text/javascript" src="js/numscroller-1.0.js"></script>
	<!-- //numscroller -->

	<!-- Owl-Carousel-JavaScript -->
	<script src="js/owl.carousel.js"></script>
	<script>
		$(document).ready(function () {
			$("#owl1-demo").owlCarousel({
				items: 3,
				lazyLoad: true,
				autoPlay: true,
				pagination: true,
			});
		});
		$('#owl1').owlCarousel({
			items: 1,
			singleItem: true,
			loop: true,
			autoPlay: true,
			autoPlayTimeout: 2000
		});
		$(document).ready(function () {
			$("#owl2-demo").owlCarousel({
				items: 3,
				lazyLoad: true,
				autoPlay: true,
				pagination: true,
			});
		});
		$('#owl2').owlCarousel({
			items: 1,
			singleItem: true,
			loop: true,
			autoPlay: true,
			autoPlayTimeout: 2000
		});
		$(document).ready(function () {
			$("#owl3-demo").owlCarousel({
				items: 3,
				lazyLoad: true,
				autoPlay: true,
				pagination: true,
			});
		});
		$('#owl3').owlCarousel({
			items: 1,
			singleItem: true,
			loop: true,
			autoPlay: true,
			autoPlayTimeout: 2000
		});
		$(document).ready(function () {
			$("#owl4-demo").owlCarousel({
				items: 3,
				lazyLoad: true,
				autoPlay: true,
				pagination: true,
			});
		});
		$('#owl4').owlCarousel({
			items: 1,
			singleItem: true,
			loop: true,
			autoPlay: true,
			autoPlayTimeout: 2000
		});
	</script>

	<!-- //Owl-Carousel-JavaScript -->

	<!-- start-smoth-scrolling -->
	<script src="js/SmoothScroll.min.js"></script>
	<script type="text/javascript" src="js/move-top.js"></script>
	<script type="text/javascript" src="js/easing.js"></script>
	<script type="text/javascript">
		jQuery(document).ready(function ($) {
			$(".scroll").click(function (event) {
				event.preventDefault();
				$('html,body').animate({ scrollTop: $(this.hash).offset().top }, 1000);
			});
		});
	</script>
	<!-- here stars scrolling icon -->
	<script type="text/javascript">
		$(document).ready(function () {
			/*
				var defaults = {
				containerID: 'toTop', // fading element id
				containerHoverID: 'toTopHover', // fading element hover id
				scrollSpeed: 1200,
				easingType: 'linear' 
				};
			*/

			$().UItoTop({ easingType: 'easeOutQuart' });

		});
	</script>
	<!-- //here ends scrolling icon -->
	<!-- start-smoth-scrolling -->

	<!-- //js-scripts -->

</body>

</html>


</body>

</html>