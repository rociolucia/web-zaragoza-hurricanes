<!-- head -->
<?php
include "includes/head.php"
?>
<!-- //head -->

<body>

<!-- menu -->
<?php
include "includes/menu.php";
	
include "Conexion/conexion.php";
?>
<!-- //menu -->
	<div class="inner-page-banner text-center">
		<div class="banner-dott3">
			<div class="container">
				<br><br><br><br>
				<h2 class="text-capitalize"></h2>
				<strong>
					<p><a href="index.php"></a></p>
				</strong>
			</div>
		</div>
	</div>
	<!-- //logo + menu -->
	<!--// header -->

	<!-- formulario de contacto -->
	<section class="contact py-5 my-lg-5">
		<div class="text-center icon"> <span><i class="fas fa-football-ball"></i></span> </div>
		<h3 class="heading text-center text-capitalize mb-5"> Donde encontranos</h3>
		<div class="container">
			<div class="row contact-grids">
				<div class="col-md-6 contact_right p-lg-5 p-4">
					<form action="PHPMailer3/enviar-email.php" name="enviar" method="post">
						<div class="w3_agileits_contact_left">
							
							<center>
								<h3 class="mb-3">Formulario de Contacto</h3>
							</center>
							<input type="text" name="name" placeholder="Tu Nombre" required="">
							<input type="email" name="email" placeholder="Tu email" required="">
							<input type="text" name="telefono" placeholder="Número de Teléfono" required="">
							<textarea placeholder="Escribe tu mensaje..." name="mensaje" required=""></textarea>
						</div>
						<div class="w3_agileits_contact_right">
							<input type="submit" value="Enviar">
						</div>
						<center><span class="text-gray">Al registrarse, usted acepta nuestros<a href="privacidad.php">Terminos &
                            Condiciones.</a></span></center>
                            <br><br>
							<center>
								<h3 class="mb-3">¡TU MENSAJE HA SIDO ENVIADO!</h3>
							</center>
						<div class="clearfix"> </div>
					</form>
				</div>
				<div class="col-md-6">
					<img src="images/contact.jpg" alt="" class="img-fluid" />
				</div>
			</div>
		</div>
	</section>
	<!-- //formulario de contacto -->

	<!-- mapa -->
	<div class="map">
		<iframe
			src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2982.08959485828!2d-0.9254848843239207!3d41.63219478874552!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xd596b5c2f191697%3A0xbd3ccb1e4e9f0da6!2sZaragoza%20Hurricanes!5e0!3m2!1ses!2ses!4v1600020849005!5m2!1ses!2ses"
			width="600" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false"
			tabindex="0"></iframe>
		<div class="agile_map_grid">
			<div class="agile_map_grid1">
				<h3>Información de Contacto</h3>
				<?php 

					$contacto = $pdo->query("SELECT * FROM contacto");

					forEach($contacto as $contac): ?>
				<ul>
					<li><i class="fa fa-map-marker" aria-hidden="true"></i><span>Dirección :</span> <?php echo $contac["Direccion"] ?></li>
					<li><i class="fa fa-envelope" aria-hidden="true"></i><span>Email :</span> <a
							href="mailto:<?php echo $contac["Email"] ?>"><?php echo $contac["Email"] ?></a></li>
					<li><i class="fa fa-phone" aria-hidden="true"></i><span>Teléfono :</span><?php echo $contac["Telefono"] ?></li>
				</ul>
				<?php endforeach;?>
			</div>
		</div>
	</div>
	<!-- //mapa -->

	<!-- footer -->
<?php
include "includes/footer.php"
?>
<!-- //footer -->


	<!-- js-scripts -->
	<?php
include "includes/script.php"
?>
<!-- //js-scripts -->


</body>

</html>