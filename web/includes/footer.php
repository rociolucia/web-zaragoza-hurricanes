	<!--footer-->
	
<footer>
	<?php
	
	include "Conexion/conexion.php";

?>
		<div class="container py-md-4 mt-md-0">
			<div class="row footer-top-w3layouts-agile py-5">
				<div class="col-md-4 footer-grid">
					<div class="footer-title">
						<h3>Donde estamos</h3>
					</div>
					<?php 

					$contacto = $pdo->query("SELECT * FROM contacto");

					forEach($contacto as $contac): ?>
					<div class="contact-info">
						<h4>Dirección :</h4>
						<p><?php echo $contac["Direccion"] ?></p>
						<div class="phone">
							<h4>Teléfono :</h4>
							<p>Teléfono: <?php echo $contac["Telefono"] ?></p>
							<p>Email : <a href="mailto:<?php echo $contac["Email"] ?>"><?php echo $contac["Email"] ?></a></p>
						</div>
					</div>
					<?php endforeach;?>
				</div>
				<div class="col-md-8 footer-grid">
					<div class="footer-title">
						<h3 class="titulo-footer">Colaboradores</h3>
					</div>
					<div class="footer-list">
					<?php 

					$colaboradores = $pdo->query("SELECT * FROM colaboradores");

					forEach($colaboradores as $colaborador): ?>

						<div class="flickr-grid">
							<a href="<?php echo $colaborador["Facebook"] ?>">
								<img src="../login/Imagenes/<?php echo $colaborador["Foto"] ?>" class="" alt="" height="100" width="220"  >
							</a>
						</div>
						<?php endforeach;?>

						<div class="clearfix"></div>
					</div>
				</div>

			</div>
		</div>
		</section>
	</footer>
	<!---->
	<div class="copyright py-3">
		<div class="container">
			<div class="copyrighttop">
				<ul>
					<li>
						<h4>Síguenos en:</h4>
					</li>
					<li>
						<a href="https://www.facebook.com/ZaragozaHurricanes/">
							<span class="fa fa-facebook"></span>
						</a>
					</li>
					<li>
						<a href="https://twitter.com/zgzhurricanes?lang=es">
							<span class="fa fa-twitter"></span>
						</a>
					</li>
					<li>
						<a href="https://www.instagram.com/zaragozahurricanes/">
							<span class="fa fa-instagram"></span>
						</a>
					</li>
					<li>
						<a href="https://www.youtube.com/channel/UC2st2sGVe1nEtfHFlSY8_AA">
							<span class="fa fa-youtube"></span>
						</a>
					</li>
				</ul>
			</div>
			<div class="copyrightbottom">
				<p>© 2020 Hurricanes Zaragoza. All Rights Reserved | Design by
					<a href="#">Bigeyes</a>
				</p>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
	<!-- //footer -->