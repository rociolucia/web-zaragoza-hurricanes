<?php
$menu= $_SERVER['PHP_SELF'];
$menu= explode("/",$menu);
$menu= end($menu);

?>

<div class="w3_agilits_banner_bootm">
		<!-- header -->
		<!-- logo + menu -->
		<div class="container">
			<div class="w3_agile_logo">
				<h1 class="heading-agileinfo" id="textoinicial">
					<a href="index.php"><img src="images/logo.png" alt="" class="imglogo" id="textclase">
						&nbsp&nbspZaragoza Hurricanes <span id="textclase">The Legend Continues</span></a>
				</h1>
			</div>
			<div class="agileits_w3layouts_nav">
				<div id="toggle_m_nav">
					<div id="m_nav_menu" class="m_nav">
						<div class="m_nav_ham w3_agileits_ham" id="m_ham_1"></div>
						<div class="m_nav_ham" id="m_ham_2"></div>
						<div class="m_nav_ham" id="m_ham_3"></div>
					</div>

				</div>
				<div id="m_nav_container" class="m_nav wthree_bg">
					<nav class="menu menu--sebastian">
						<ul id="m_nav_list" class="m_nav menu__list">
							<li class="m_nav_item menu__item <?php if($menu=="index.php"){ echo "menu__item--current";}?>" id="m_nav_item_1">
								<a href="index.php" class="menu__link"> Inicio </a>
							</li>
							<li class="m_nav_item menu__item <?php if($menu=="temporadas.php"){ echo "menu__item--current";}?>" id="m_nav_item_1">
								<a href="temporadas.php" class="menu__link"> Nosotros </a>
							</li>

							<li class="m_nav_item menu__item <?php if($menu=="categorias.php"){ echo "menu__item--current";}?>" id="m_nav_item_1">
								<a href="categorias.php" class="menu__link">Categorías</a>
							</li>
							<li class="m_nav_item menu__item <?php if($menu=="calendario.php"){ echo "menu__item--current";}?>" id="m_nav_item_1">
								<a href="calendario.php" class="menu__link">Calendario</a>
							</li>
							<li class="m_nav_item menu__item <?php if($menu=="revista.php"){ echo "menu__item--current";}?>" id="m_nav_item_1">
								<a href="revista.php" class="menu__link">Revista</a>
							</li>
							<li class="m_nav_item menu__item <?php if($menu=="contact.php"){ echo "menu__item--current";}?>" id="m_nav_item_1">
								<a href="contact.php" class="menu__link"> Contacto </a>
							</li>
						</ul>
					</nav>
				</div>
			</div>
		</div>
	</div>
