	<!-- js -->
	<script type="text/javascript" src="js/jquery-2.2.3.min.js"></script>
	<script type="text/javascript" src="js/bootstrap.js"></script> <!-- Necessary-JavaScript-File-For-Bootstrap -->
	<script type="text/javascript" src="js/numscroller-1.0.js"></script>
    <script type="text/javascript" src="assets3/js/all.js"></script>
    <script type="text/javascript" src="assets3/js/app.js"></script>
    <script type="text/javascript" src="assets3/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="assets3/js/imagezoom.js"></script>
    <script type="text/javascript" src="assets3/js/jquery.flexslider.js"></script>
    <script type="text/javascript" src="assets3/js/jquery.magnific-popup.js"></script>
    <script type="text/javascript" src="assets3/js/jquery.prettyPhoto.js"></script>
    <script type="text/javascript" src="assets3/jquery.quicksand.js"></script>
    <script type="text/javascript" src="assets3/jquery-2.1.4.min.js"></script>
	<!-- //js -->

	<!-- for banner js file-->
	<script type="text/javascript" src="js/jquery.zoomslider.min.js"></script><!-- zoomslider js -->
	<!-- //for banner js file-->

	<!-- /numscroller -->
	<script type="text/javascript" src="js/numscroller-1.0.js"></script>
	<!-- //numscroller -->

	<!--pop-up-box -->
	<script src="js/jquery.magnific-popup.js"></script>
	<script>
		$(document).ready(function () {
			$('.popup-with-zoom-anim').magnificPopup({
				type: 'inline',
				fixedContentPos: false,
				fixedBgPos: true,
				overflowY: 'auto',
				closeBtnInside: true,
				preloader: false,
				midClick: true,
				removalDelay: 300,
				mainClass: 'my-mfp-zoom-in'
			});
		});
	</script>
	<!-- //pop-up-box -->

	<!-- Owl-Carousel-JavaScript -->
	<script src="js/owl.carousel.js"></script>
	<script>
		$(document).ready(function () {
			$("#owl-demo").owlCarousel({
				items: 3,
				lazyLoad: true,
				autoPlay: true,
				pagination: true,
			});
		});
	</script>
	<!-- //Owl-Carousel-JavaScript -->

	<!-- navigation  -->
	<script src="js/main.js"></script>
	<!-- //navigation -->

	<!-- services FlexSlider -->
	<script defer src="js/jquery.flexslider.js"></script>
	<script>
		$(window).load(function () {
			$('#carousel').flexslider({
				animation: "slide",
				controlNav: false,
				animationLoop: true,
				slideshow: false,
				itemWidth: 102,
				itemMargin: 5,
				asNavFor: '#slider'
			});

			$('#slider').flexslider({
				animation: "slide",
				controlNav: false,
				animationLoop: true,
				slideshow: true,
				sync: "#carousel",
				start: function (slider) {
					$('body').removeClass('loading');
				}
			});
		});
	</script>
	<!-- // services FlexSlider -->

	<!-- start-smoth-scrolling -->
	
	<script type="text/javascript" src="js/move-top.js"></script>
	<script type="text/javascript" src="js/easing.js"></script>
	<script type="text/javascript">
		jQuery(document).ready(function ($) {
			$(".scroll").click(function (event) {
				event.preventDefault();
				$('html,body').animate({ scrollTop: $(this.hash).offset().top }, 1000);
			});
		});
	</script>
	<!-- here stars scrolling icon -->
	<script type="text/javascript">
		$(document).ready(function () {
			/*
				var defaults = {
				containerID: 'toTop', // fading element id
				containerHoverID: 'toTopHover', // fading element hover id
				scrollSpeed: 1200,
				easingType: 'linear' 
				};
			*/

			$().UItoTop({ easingType: 'easeOutQuart' });

		});
	</script>
	<!-- //here ends scrolling icon -->
	<!-- start-smoth-scrolling -->