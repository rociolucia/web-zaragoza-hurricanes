<!-- head -->
	<?php include("includes/head.php") ?>
<!-- //head -->

<body>

<!-- menu -->
<?php
	include "includes/menu.php";
	
	include "Conexion/conexion.php";

?>
<!-- //menu -->

	

	<!--slider-->
	<div id="demo-1" data-zs-src='["images/b1.jpg", "images/b3.jpg", "images/b2.jpg", "images/b4.jpg"]'
		data-zs-overlay="dots">
		<div class="demo-inner-content">

			<!--información slider-->
			<div class="w3-banner-head-info">
				<div class="container">
					<div class="banner-text">
						
						<h2 class="mb-lg-5 mb-3">El verdadero <span>Football</span><br> Comienza aquí</h2>

					</div>
				</div>
			</div>
			<!--//información slider-->
		</div>
	</div>
	<!--//slider-->

	<!-- historia + equipo -->
	<section class="about-w3ls py-5">
		<div class="container pt-xl-5 pb-lg-3">
			<div class="row">
				<div class="col-lg-7">
					<iframe src="https://www.youtube.com/embed/wMgNc_bedrM" frameborder="0"
						allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen
						class="img-section4 img-fluid video-home"></iframe>
				</div>
				<div class="col-lg-5 section-4">
					<div class="agil_mor">
						<h2 class="heading-agileinfo titulos-home">Nuestra <span> Historia</span></h2>
						<p class="vam texto-home text-gray ">El football en Aragón nace en el año 1989, con la creación
							de los
							Zaragoza Lions, equipo creado en la base aérea americana. Los Lions se mantuvieron en la
							élite en sus 15 años de historia, llegando a jugar la liga europea, y manteniendo un alto
							nivel en la liga nacional en todos sus años de existencia. </p>
						<a href="temporadas.php" class="mt-3">Leer más</a>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="about-w3ls py-5">
		<div class="container pt-xl-5 pb-lg-3">
			<div class="row">
				<div class="col-lg-5 section-5">
					<div class="agil_mor">
						<h2 class="heading-agileinfo">Nuestro <span>Equipo</span></h3>
						<p class="vam text-gray">El equipo de los Lions es el origen de nuestro equipo actual. Tras la
							desaparición del equipo en 2004, un año después nacían los Zaragoza Hurricanes. Desde el
							comienzo, el grupo destacó por su capacidad y proyección, llegando en su primer año a la
							final de conferencia de la Segunda División de la Liga Nacional. </p>

					</div>
				</div>
				<div class="col-lg-7">
				<iframe src="https://www.youtube.com/embed/FGqexyRcsnY" frameborder="0"
				allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen
				class="img-section4 img-fluid video-home"></iframe>
				</div>

			</div>
		</div>
	</section>
	<!-- //historia + equipo -->

	<!-- info categorias -->
	<div class="more-services py-lg-5">
		<div class="container py-5">
			<div class="title-section pb-sm-5 pb-3">
				<h3 class="heading-agileinfo text-center pb-4">Zaragoza <span> Hurricanes</span></h3>
			</div>
			<div class="row grid">
				<div class="col-lg-3 col-6 more-services-1">
					<figure class="effect-1">
						<img src="images/p1.jpg" alt="img" class="img-fluid" />
						<h4>Senior</h4>
						<p> Cada vez más fuerte, compacto, independiente, y con posibilidad de alcanzar cotas más altas
							cada temporada.<br><br><br><a href="categorias.php#senior" class="mt-3 mas">Más</a></p>
					</figure>
				</div>
				<div class="col-lg-3 col-6 more-services-1">
					<figure class="effect-1">
						<img src="images/p2.jpg" alt="img" class="img-fluid" />
						<h4>Femenino</h4>
						<p> En el Club Hurricanes Zaragoza no hay límites ni diferencias entre sexos, solo unidad,
							compromiso y mucha ilusión.<br><br><br><a href="categorias.php#femenino"
								class="mt-3 mas">Más</a></p>
					</figure>
				</div>

				<div class="col-lg-3 col-6 more-services-1">
					<figure class="effect-1">
						<img src="images/p3.jpg" alt="img" class="img-fluid" />
						<h4>Junior</h4>
						<p>Valores, sentimiento de pertenencia, experiencias inolvidables... en esta disciplina todos
							los jóvenes tienen cabida.<br><br><br><a href="categorias.php#junior"
								class="mt-3 mas">Más</a></p>
					</figure>
				</div>
				<div class="col-lg-3 col-6 more-services-1">
					<figure class="effect-1">
						<img src="images/p4.jpg" alt="img" class="img-fluid" />
						<h4>Hurricanes Academy</h4>
						<p> Para menores de 14 años. Esta disciplina retiene las características de juego del fútbol americano pero no emplea el
							contacto para finalizar las jugadas.<br><br><a href="categorias.php#flag"
								class="mt-3 mas">Más</a>
						</p>
					</figure>
				</div>
			</div>

		</div>
	</div>
	<!-- //info categorias -->

	<!-- welcome pack -->
	<div class="testimonials py-lg-5">
		<div class="container py-5">
			<div class="title-section pb-sm-5 pb-3">
				<h3 class="heading-agileinfo text-center pb-4">Welcome <span> Pack</span></h3>
			</div>
			<div class="agileits-services-row row text-center">

				<?php 

					$welcomepack = $pdo->query("SELECT * FROM welcomepack");

					forEach($welcomepack as $welcome): ?>
					
				<div class="col-lg-3 col-md-6">
					<div class="agileits-services-grids">
						<span class="fa fa-thumbs-up"></span>
						<h4 class="text-white"><?php echo $welcome["Titulo"] ?>
						</h4>
						<label></label>
						<p class="text-white"><?php echo $welcome["Descripcion"] ?> </p>

					</div>
				</div>
				<?php endforeach;?>
				<div class="col text-center">
					<br><br><br><a class="boton_personalizado" href="welcome.php">SOLICITAR</a>
				</div>
			</div>
		</div>
	</div>
	</div>
	<!--// welcome pack -->

	<!-- patrocinadores -->
	<div class="team py-lg-5">
		<div class="container py-5">
		
			<div class="row team-bottom">
				<?php 

					$patrocinadores = $pdo->query("SELECT * FROM patrocinadores");

					forEach($patrocinadores as $patrocinador): ?>
						
						
							<div class="col-sm-3 team-grid mt-sm-0 my-5 ">
							<img src="../login/Imagenes/<?php echo $patrocinador["Foto"] ?>" class=" img-thumbnail" alt="" style=”max-width:100%;width:auto;height:auto;”>
							<div class="caption">
								<div class="team-text">
									<h4><?php echo $patrocinador["Nombre"] ?></h4>
								</div>
								<ul>
									<li>
										<a href="<?php echo $patrocinador["Facebook"] ?>">
											<span class="fa fa-facebook f1"></span>
										</a>
									</li>
									<li>
										<a href="<?php echo $patrocinador["Twitter"] ?>">
											<span class="fa fa-twitter f2"></span>
										</a>
									</li>
									<li>
										<a href="<?php echo $patrocinador["Google"] ?>">
											<span class="fa fa-google-plus f3"></span>
										</a>
									</li>
								</ul>
							</div>
						</div>
					<?php endforeach;?>
			</div>
		</div>
	</div>
	<!-- //patrocinadores -->

	<!-- ultimas noticias -->
	<div class="video-choose-agile py-lg-5">
		<div class="container py-5">
			<div class="title-section pb-sm-5 pb-3">
				<h3 class="heading-agileinfo text-center pb-4">Últimas <span>Noticias</span></h3>
			</div>
			<div class="row">
				<div class="col-lg-5 events">
					<div class="events-w3ls">
					<?php 

					$noticias = $pdo->query("SELECT * FROM noticias");

					forEach($noticias as $noticia): ?>
					<div class="d-flex mt-4">
					

						<div class="col-sm-2 col-3 events-up p-2 text-center">

								<h5 class="font-weight-bold"><?php echo $noticia["Dia"] ?>
									<span class="border-top font-weight-light pt-2 mt-2"><?php echo $noticia["Mes"] ?></span>
								</h5>
							</div>
							<div class="col-sm-10 col-9 events-right">
								<a href="<?php echo $noticia["Enlace"] ?>"
									class="text-a"><strong><?php echo $noticia["Titulo"] ?></strong></a>
								<ul class="list-unstyled">
									<li class="my-2">
										<span class="fa fa-clock-o mr-2"></span><?php echo $noticia["Fecha"] ?></li>
									<li>
										<span class="fa fa-map-marker mr-2"></span><?php echo $noticia["Publicacion"] ?></li>
								</ul>
							</div>
						</div>
						<?php endforeach;?>

					</div>
					
				</div>
				<div class="col-lg-7 video">
					<img src="images/bg2.jpg" class="img-fluid" alt="" />
				</div>
			</div>
		</div>
	</div>
	<!-- //ultimas noticias -->

	<!-- footer -->
	<?php
include "includes/footer.php"
?>
<!-- //footer -->

	<!-- js-scripts -->
	<?php
include "includes/script.php"
?>
<!-- //js-scripts -->


</body>

</html>