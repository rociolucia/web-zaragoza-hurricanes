<!DOCTYPE html>
<html lang="es">

<head>
	<title>Zaragoza Hurricanes – Fútbol Americano en Zaragoza</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta charset="utf-8" />
	<meta name="keywords"
		content="Zaragoza Hurricanes, Equipo de futbol americano de Zaragoza, football, futbol, gobierno de Aragon, masterd, zaragoza deporte, gran cafe zaragozano, Los Lions, calendario deportivo, facebook, instagram, twiter, youtube, club de football" />
	<!-- script/ carga de página -->
	<script>
		addEventListener("load", function () {
			setTimeout(hideURLbar, 0);
		}, false);

		function hideURLbar() {
			window.scrollTo(0, 1);
		}
	</script>
	<!-- //script/ carga de página -->
	<!-- estilos carrusel de imagenes -->
	<link rel="stylesheet" href="css/owl.carousel.css" type="text/css" media="all" />
	<link href="css/popuo-box.css" rel="stylesheet" type="text/css" media="all" />
	<link rel="stylesheet" href="css/flexslider.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="css/slider.css" type="text/css" media="all" />
	<!-- //estilos carrusel de imagenes -->
	<!-- estilos nosotros -->
	<link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/bootstrap2.css">
    <link rel="stylesheet" href="css/style.css" type="text/css" media="all" />
    <link rel="stylesheet" href="assets3/css/style-starter.css" type="text/css" media="all" />
    <link rel="stylesheet" href="assets3/fonts/fontawesome-webfont.svg" type="text/css" media="all" />
    <link rel="stylesheet" href="assets3/fonts/fontawesome-webfont.svg" type="text/css" media="all" />
    <link href="css/style4.css" type="text/css" rel="stylesheet" media="all">
	<link href="css/bootstrap.css" type="text/css" rel="stylesheet" media="all">
	<link href="css/style.css" type="text/css" rel="stylesheet" media="all">
	<link rel="stylesheet" href="css/style2.css" type="text/css" media="all" />
	<link href="css/style4.css" type="text/css" rel="stylesheet" media="all">
	<!-- //estilos nosotros -->
	<!-- iconos -->
	<link rel="stylesheet" href="css/fontawesome-all.css">
	<link href="css/fontawesome-all.min.css" rel="stylesheet">
	<!-- //iconos -->
	<!-- tipografias -->
	<link href="//fonts.googleapis.com/css?family=Ubuntu:300,300i,400,400i,500,500i,700,700i" rel="stylesheet">
	<link
		href="//fonts.googleapis.com/css?family=Montserrat:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i&amp;subset=cyrillic,cyrillic-ext,latin-ext,vietnamese"
		rel="stylesheet">
	<link
		href="//fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i&amp;subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese"
		rel="stylesheet">
	<!-- //tipografias -->
</head>

<body>

<!-- menu -->
<?php
include "includes/menu.php";
include "Conexion/conexion.php";
?>
<!-- //menu -->
	<div class="inner-page-banner text-center">
		<div class="banner-dott3">
			<div class="container">
				<br><br><br><br>
				<h2 class="text-capitalize"></h2>
				<strong>
					<p><a href="index.php"></a></p>
				</strong>
			</div>
		</div>
	</div>
	<!-- //logo + menu -->
	<!--// header -->


		<!-- formulario para jugadores -->
		<section class="text py-5" id="register">
			<div class="container py-md-5 py-3 text-center">
				<div class="row">
					<div class="col-12">
						<h2 class="heading-agileinfo text-center pb-4 text-white">Partidos disputados <span>LNFA Femenina 7</span></h2>
                        <?php 

					$p_femenino = $pdo->query("SELECT * FROM p_femenino");

					forEach($p_femenino as $p_femeni): ?>
                        <img src="../login/Imagenes/<?php echo $p_femeni["Foto"] ?>" class="img-fluid" alt="">
						
						<?php endforeach;?>	
    </div>
						
				</div>
			</div>
    </section>
			<!-- //formulario para jugadores -->

	<!-- footer -->
<?php
include "includes/footer.php"
?>
<!-- //footer -->



	<!-- js-scripts -->
	<?php
include "includes/script.php"
?>
<!-- //js-scripts -->


</body>

</html>
</html>