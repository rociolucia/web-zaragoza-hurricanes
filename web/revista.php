﻿<!-- head -->
<?php
include "includes/head.php"
?>
<!-- //head -->

<body>

<!-- menu -->
<?php
include "includes/menu.php";
	
include "Conexion/conexion.php";
?>
<!-- //menu -->
    <div class="inner-page-banner text-center">
        <div class="banner-dott3">
            <div class="container">
                <br><br><br><br>
                <h2 class="text-capitalize"></h2>
                <strong>
                    <p><a href="index.php"></a></p>
                </strong>
            </div>
        </div>
    </div>
    <!-- //logo + menu -->
    <!--// header -->

    <!-- Single -->
    <section class="contact py-5 my-lg-5">
        <div class="text-center icon"> <span><i class="fas fa-football-ball"></i></span> </div>
        <h3 class="heading text-center text-capitalize mb-5"> Nuestra Revista</h3>
        <div class="container">
            <div class="row inner-sec">
                <!--left-->
                <div class="col-lg-12 left-blog-info text-left">

                <iframe src="https://cdn.flipsnack.com/widget/v2/widget.html?hash=7csgnnprzp" width="100%" height="480" 
                seamless="seamless" scrolling="no" frameBorder="0" allowFullScreen></iframe>
                

                </div>

                <div class="col-md-6">
                    <div class="comment-top textnow">
                        <h4>Revistas anteriores</h4>
                        <?php 

					$revista = $pdo->query("SELECT * FROM revista");

					forEach($revista as $revi): ?>
                        <div class="media mt-3">
                            <img src="images/revista4.jpg" alt="" class="img-fluid">
                            <div class="media-body">
                                <a href="<?php echo $revi["Foto"] ?>" class="text-enlace"><?php echo $revi["Nombre"] ?></a>
                            </div>
                        </div>
                        <?php endforeach;?>

                    </div>
                </div>
                <aside class="col-lg-4 right-blog-con text-right">
                    <div class="right-blog-info text-left">



                        <br><br><br><br><br>
                        <h4 class="texto-azul">Último Post</h4>
                        <div class="blog-grids">
                            <div class="blog-grid-left">
                                <a
                                    href="https://amp.marca.com/claro-mx/otros-deportes/futbol-americano/2020/07/10/5f0888ace2704e8b418b45b2.html">
                                    <img src="images/a36.jpg" class="img-fluid" alt="">
                                </a>
                            </div>
                            <div class="blog-grid-right">


                                <p class="text-gray">¿Qué significan los apodos de los 32 equipos de la NFL?</p>

                            </div>

                        </div>
                    </div>
            </div>

            </aside>
            <!-- //post-grids-->
            <br><br>
            <section class="w3l-customers-sec-6">
                <div class="customers-sec-6-cintent py-5">
                    <!-- /customers-->
                    <div class="container py-lg-5">
                        <h3 class="hny-title text-center mb-0 ">Comentarios <span class="texto-azul">Hurricanes</span>
                        </h3>
                        <p class="mb-5 text-center">Que dicen nuestros jugadores</p>
                        <div class="row customerhny my-lg-5 my-4">
                            <div class="col-md-12">
                                <div id="customerhnyCarousel" class="carousel slide" data-ride="carousel">

                                    <ol class="carousel-indicators">
                                        <li data-target="#customerhnyCarousel" data-slide-to="0" class="active"></li>
                                        <li data-target="#customerhnyCarousel" data-slide-to="1"></li>
                                    </ol>
                                    <!-- Carousel items -->
                                    <div class="carousel-inner">

                                        <div class="carousel-item active">
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <div class="customer-info text-center">
                                                        <div class="feedback-hny">
                                                            <span class="fa fa-quote-left"></span>
                                                            <p class="feedback-para">Los Hurricanes son un refugio para
                                                                cualquiera donde los
                                                                principales valores son Respeto y Esfuerzo.</p>
                                                        </div>
                                                        <div class="feedback-review mt-4">
                                                            <img src="assets3/images/c1.jpg" class="img-fluid" alt="">
                                                            <h5>Lara</h5>

                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="customer-info text-center">
                                                        <div class="feedback-hny">
                                                            <span class="fa fa-quote-left"></span>
                                                            <p class="feedback-para">Un eslabón de la cadena Hurricanes;<br>
                                                                todos importantes, todos necesarios.
                                                            </p>
                                                        </div>
                                                        <div class="feedback-review mt-4">
                                                            <img src="assets3/images/c6.jpg" class="img-fluid" alt="">
                                                            <h5>Latxo Pérez</h5>

                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="customer-info text-center">
                                                        <div class="feedback-hny">
                                                            <span class="fa fa-quote-left"></span>
                                                            <p class="feedback-para">Hurricanes es una pasión, son la fuerza que me da el
                                                                equipo, la amistad que tenemos y la familia que somos.
                                                            </p>
                                                        </div>
                                                        <div class="feedback-review mt-4">
                                                            <img src="assets3/images/c2.jpg" class="img-fluid" alt="">
                                                            <h5>Natasha Ceballos</h5>

                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="customer-info text-center">
                                                        <div class="feedback-hny">
                                                            <span class="fa fa-quote-left"></span>
                                                            <p class="feedback-para">Play like a champion today. <br><br>1 Team
                                                                1 Family.</p><br>
                                                        </div>
                                                        <div class="feedback-review mt-4">
                                                            <img src="assets3/images/c7.jpg" class="img-fluid" alt="">
                                                            <h5>Isaac Buján</h5>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--.row-->
                                        </div>
                                        <!--.item-->

                                        <div class="carousel-item">
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <div class="customer-info text-center">
                                                        <div class="feedback-hny">
                                                            <span class="fa fa-quote-left"></span>
                                                            <p class="feedback-para">Hurricanes es familia y forma de
                                                                vida.</p><br><br>
                                                        </div>
                                                        <div class="feedback-review mt-4">
                                                            <img src="assets3/images/c3.jpg" class="img-fluid" alt="">
                                                            <h5>Natalia Muñio</h5>

                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="customer-info text-center">
                                                        <div class="feedback-hny">
                                                            <span class="fa fa-quote-left"></span>
                                                            <p class="feedback-para">Permítete caer, pero no rendirte y
                                                                volver a levantarte.</p><br><br>
                                                        </div>
                                                        <div class="feedback-review mt-4">
                                                            <img src="assets3/images/c8.jpg" class="img-fluid" alt="">
                                                            <h5>Ángel Marín</h5>

                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="customer-info text-center">
                                                        <div class="feedback-hny">
                                                            <span class="fa fa-quote-left"></span>
                                                            <p class="feedback-para">Para mi Hurricanes es
                                                                responsabilidad, lealtad, respeto, libertad y
                                                                autoestima. </p><br>
                                                        </div>
                                                        <div class="feedback-review mt-4">
                                                            <img src="assets3/images/c4.jpg" class="img-fluid" alt="">
                                                            <h5>Inés Koulel</h5>

                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="customer-info text-center">
                                                        <div class="feedback-hny">
                                                            <span class="fa fa-quote-left"></span>
                                                            <p class="feedback-para">Una familia que me ha hecho crecer personalmente y descubrir un
                                                                deporte que me ha cautivado.</p>
                                                        </div>
                                                        <div class="feedback-review mt-4">
                                                            <img src="assets3/images/c5.jpg" class="img-fluid" alt="">
                                                            <h5>Ana Leonte</h5>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--.row-->
                                        </div>
                                        <!--.item-->

                                    </div>
                                    <!--.carousel-inner-->
                                </div>
                                <!--.Carousel-->

                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- //customers-->
            <br><br>
            <section class="w3l-subscription-6">
                <!--/customers -->
                <div class="subscription-infhny">
                    <div class="container-fluid">

                        <div class="subscription-grids row">

                            <div class="subscription-right form-right-inf col-lg-6 p-md-5 p-4">
                                <div class="p-lg-5 py-md-0 py-3">
                                    <h3 class="hny-title">Subscríbete para recibir<span> nuestra revista!</span></h3>
                                    <p>Todas las noticias más relevantes sobre el club y la liga.</p>

                                    <form action="PHPMailer4/enviar-email.php" name="enviar" method="post">
                                        <div class="forms-gds">
                                            <div class="form-input">
                                                <input type="email" name="email" placeholder="Tu email aquí" required="">
                                            </div>
                                            <div class="form-input"><button class="btn">Solicitar</button></div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div class="subscription-left forms-25-info col-lg-6 ">

                            </div>
                        </div>

                        <!--//customers -->
                    </div>
            </section>
            <!-- //customers-6-->


        </div>
        </div>
    </section>
    <!-- //Single -->

  	<!-- footer -->
	<?php
include "includes/footer.php"
?>
<!-- //footer -->


	<!-- js-scripts -->
	<?php
include "includes/script.php"
?>
<!-- //js-scripts -->


</body>

</html>