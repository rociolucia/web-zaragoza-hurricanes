<!-- head -->
<?php include("includes/head.php") ?>
<!-- //head -->

<body>

<!-- menu -->
<?php
include "includes/menu.php";
	
include "Conexion/conexion.php";
?>
<!-- //menu -->
	<div class="inner-page-banner text-center">
		<div class="banner-dott3">
			<div class="container">
				<br><br><br><br>
				<h2 class="text-capitalize"></h2>
				<strong>
					<p><a href="index.php"></a></p>
				</strong>
			</div>
		</div>
	</div>
	<!-- //logo + menu -->
	<!--// header -->

	<!-- seccion objetivos -->
	<div class="agileits-services py-lg-5">
		<div class="container py-5">
			<div class="title-section pb-sm-5 pb-3">
				<h2 class="heading-agileinfo text-center pb-4">Nuestros <span>Objetivos</span></h2>
			</div>
			<div class="agileits-services-row row text-center">
				<div class="col-lg-3 col-md-6">
					<div class="agileits-services-grids">
						<span class="fa fa-object-group"></span>
						<h4>Compartir Metas
						</h4>
						<label></label>
						<p class="text-gray">Tener un mismo objetivo, una orientación conjunta clara, es lo que da
							“unidad” y “dirección”
							a
							nuestro equipo. <br>Lograr «algo» que a todos nos interesa. <br>Es lo
							que nos permite mantener un compromiso común.
						</p>

					</div>
				</div>
				<div class="col-lg-3 col-md-6">
					<div class="agileits-services-grids mt-lg-0 mt-md-0 mt-5">
						<span class="fa fa-cog"></span>
						<h4>Dividir Tareas
						</h4>
						<label></label>
						<p class="text-gray">Determinamos las tareas que se
							encomienda a cada miembro del grupo y los plazos en que las deben realizar, clarificando las
							cuestiones confusas. Todos sabemos «qué» hay que hacer «por qué» y para «cuándo» hacerlo.
						</p>

					</div>
				</div>
				<div class="col-lg-3 col-md-6">
					<div class="agileits-services-grids mt-lg-0 mt-5">
						<span class="fa fa-line-chart"></span>
						<h4>Máxima Eficiencia
						</h4>
						<label></label>
						<p class="text-gray">Nuestra finalidad es ser lo más eficientes y obtener los mejores resultados
							posibles. La eficiencia es absolutamente necesaria en un equipo pero no es sufi­ciente,
							se requiere también una cohesión que dé unidad y estabilidad a los
							miembros del equipo a largo plazo. </p>

					</div>
				</div>
				<div class="col-lg-3 col-md-6  mt-lg-0 mt-5">
					<div class="agileits-services-grids">
						<span class="fa fa-users"></span>
						<h4>Mismos Valores
						</h4>
						<label></label>
						<p class="text-gray">La esencia de nuestro equipo es compartir un espíritu ganador, así como
							compartir infor­mación,
							conocimientos, experiencias, valores, opiniones, vivencias, éxitos y fra­casos entre todos
							los miembros del equipo y así
							lograr un objetivo común.</p>

					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- //seccion objetivos -->


	<!-- seccion temporadas -->
	<section class="about-bottom" id="about">
		<div class="container py-md-5 py-3  subir">
			<h5 class="heading mb-2 mas">Zaragoza Hurricanes</h5>
			<h2 class="heading-agileinfo2 mt-md-0 mt-2 mas2">La Historia de Nuestro Equipo</h2><br>
			<div class="row">
				<div class="col-lg-6 left-img">
					<img src="images/temporadas1.0.jpg" class="img-fluid" alt="" />
				</div>
				<div class="col-lg-6 mt-lg-0 mt-4">
					<div class="row inner-heading">
						<div class="col-md-2  subir3"></div>
						<div class="col-md-10 subir2">
							<h4 class="heading-agileinfo2 mt-md-0 mt-2">TEMPORADAS<span> 2011 A 2014</span></h4>
							<p class="mt-3 text-gray textos">El equipo de los Lions es el origen de nuestro equipo
								actual. Tras
								la
								desaparición del equipo en 2004, un año después nacían
								los Zaragoza Hurricanes. Desde el comienzo, el grupo destacó por su capacidad y
								proyección, llegando en su primer año a la final de conferencia
								de la Segunda División de la Liga Nacional, hecho que se repetiría dos años después.<br>
								Tras estos primeros años de asentamiento, en la temporada 2011 – 2012 el club consigue
								introducir sus tres categorías en competiciones
								nacionales: senior, junior y cadete, siendo este último de formato mixto ya que así lo
								permitía la categoría. Como resultado del trabajo de
								todos los componentes de los tres equipos, dirigidos por el coach Gustavo Joaquín todos
								ellos, se consiguen extraordinarios resultados quedando
								el equipo senior tercero de España, el junior subcampeón nacional y el cadete campeón
								nacional.

								Sin embargo a pesar de esos buenos resultados en la campaña 2011 – 2012, el club pasaría
								dos temporadas duras en cuanto a resultados compitiendo
								sólo en categoría senior</p>
						</div>
					</div>
				</div>
			</div>
			<div class="row mt-5">
				<div class="col-lg-6 mt-lg-0 mt-4">
					<div class="row inner-heading">

						<div class="col-md-10">
							<h4 class="heading-agileinfo2 mt-md-0 mt-2">TEMPORADA<span> 2014 A 2015</span></h4>
							<p class="mt-3 text-gray">Tras estas dos temporadas se da un cambio de rumbo y aprovechando
								el décimo
								aniversario del club como reclamo y bajo la dirección
								de José Javier Villa y Óscar Carné, durante la temporada 2014 – 2015, se acometen
								diversas acciones con la intención de revitalizar el equipo:
								formación de equipo femenino, fichaje de un jugador americano, equipaciones nuevas y
								traslado del club a Calatayud.

								A pesar de un esperanzador comienzo, el año no acabó bien. Una plaga de lesiones sumado
								al cambio generacional producido que dejó un equipo
								muy joven y reducido, y el mal año del equipo femenino, hizo que una temporada que
								comenzaba de manera brillante acabara poniendo a prueba la
								entereza de la directiva, que tras todos los reveses sufridos tuvo que tomar la decisión
								de descender y comenzar un nuevo proyecto desde cero.</p>
						</div>
					</div>
				</div>
				<div class="col-lg-6 left-img">
					<img src="images/temporadas8.0.jpg" class="img-fluid" alt="" id="textclase" />
				</div>
			</div>
		</div>

		<div class="container py-md-5 py-3  subir">

			<div class="row">
				<div class="col-lg-6 left-img">
					<img src="images/temporadas2.0.jpg" class="img-fluid" alt="" />
				</div>
				<div class="col-lg-6 mt-lg-0 mt-4">
					<div class="row inner-heading">
						<div class="col-md-2  subir3"></div>
						<div class="col-md-10 subir2">
							<h4 class="heading-agileinfo2 mt-md-0 mt-2">TEMPORADA<span> 2015 A 2016</span></h4>
							<p class="mt-3 text-gray">En la temporada 2015 – 2016 se inició una nueva etapa de cambio y
								mejora del
								club. Con nuevo staff técnico formado por Darío Latre como entrenador de los Zaragoza
								Hurricanes, las reincorporaciones de Isaac Buján y Alberto López como asistentes, nueva
								directiva y nueva plantilla de jugadores, se realizó una temporada brillante llena de
								victorias.
								Pero ésta también fue una difícil temporada, pues a finales de 2015, el 28 de noviembre,
								se conoce la noticia del fallecimiento de Adrián Santamaría, antiguo componente de los
								Zaragoza Hurricanes. Para honrar su memoria, esta misma temporada en el día de su
								cumpleaños, se crea el Memorial Adrián Santamaría.</p>
						</div>
					</div>
				</div>
			</div>
			<div class="row mt-5">
				<div class="col-lg-6 mt-lg-0 mt-4">
					<div class="row inner-heading">

						<div class="col-md-10">
							<h4 class="heading-agileinfo2 mt-md-0 mt-2">TEMPORADA<span> 2016 A 2017</span></h4>
							<p class="mt-3 text-gray">La temporada 2016 – 2017 se convierte en la consolidación de este
								nuevo
								equipo formado el año anterior. Con cambio en el staff técnico, formado por Isaac Buján,
								Israel Hernandez y Jesús Sánchez, los jugadores que se mantiene de la temporada pasada y
								la reincorporación de antiguos jugadores que deciden volver a formar parte de la
								plantilla ilusionados con el proyecto, se compacta un sólido bloque que termina el año
								invicto y proclamándose campeón de la Liga Norte.

								Otra muestra de la buena salud del proyecto en este año es la formación, tras varios
								años carentes de él, del equipo junior de Zaragoza Hurricanes.</p>
						</div>
					</div>
				</div>
				<div class="col-lg-6 left-img">
					<img src="images/temporadas6.0.jpg" class="img-fluid" alt="" id="textclase" />
				</div>
			</div>
		</div>

		<div class="container py-md-5 py-3  subir">

			<div class="row">
				<div class="col-lg-6 left-img">
					<img src="images/temporada5.0.jpg" class="img-fluid" alt="" />
				</div>
				<div class="col-lg-6 mt-lg-0 mt-4">
					<div class="row inner-heading">
						<div class="col-md-2  subir3"></div>
						<div class="col-md-10 subir2">
							<h4 class="heading-agileinfo2 mt-md-0 mt-2">TEMPORADA<span> 2017 A 2018</span></h4>
							<p class="mt-3 text-gray">El crecimiento de nuestro equipo ha sido constante, desde su
								creación, y ha
								ido de la mano del aumento de repercusión del football en Aragón. De este modo, hemos
								visto como surgían nuevos equipos, que al igual que el nuestro, tienen su origen en los
								Zaragoza Lions de 1989. Esto provocó que algunos equipos ya existentes notasen algunas
								carencias en sus filas debido al cambio de equipo de algunos jugadores, y se propusieran
								opciones de unión entre clubs. Esta posibilidad, concretamente, se dio entre Hornets y
								Hurricanes, quienes este 2017 llevaron a cabo una fusión, convirtiéndose en los
								Hurricanes que conocemos hoy en día. El equipo de Hurricanes, y su proyección mostrada
								en años anteriores, hacía del equipo una opción atractiva y sólida para Hornets. Esta
								unión ha convertido al equipo en un grupo más fuerte, más compacto, más independiente, y
								con la posibilidad de alcanzar cotas todavía más altas cada temporada.</p>
						</div>
					</div>
				</div>
			</div>
			<div class="row mt-5">
				<div class="col-lg-6 mt-lg-0 mt-4">
					<div class="row inner-heading">

						<div class="col-md-10">
							<h4 class="heading-agileinfo2 mt-md-0 mt-2">TEMPORADA<span> 2018 A 2019</span></h4>
							<p class="mt-3 text-gray p2">DEBUT EN LA SERIE A.<br><br> Después de varios campeonatos en
								la
								Liga Norte,
								Zaragoza Hurricanes vuelve a competir en la liga nacional Serie A.
								Los técnicos seguirían siendo Isaac Buján y Jesús Sánchez. La novedad esta al incorporar
								a los jugadores mexicanos Alcides Benítez y Edgar Argüello en el que aportarían
								conocimientos a los jugadores nacionales y poder mejorar el nivel. La temporada se saldó
								con 2 victorias en temporada regular, suficiente para el pase a los playoffs.
								Zaragoza Hurricanes hace historia ya que nunca antes llegó a cuartos de final.El
								proyecto que se inició hace ya 4 años sigue dando los resultados deseados</p>
						</div>
					</div>
				</div>
				<div class="col-lg-6 left-img">
					<img src="images/temporada6.0.png" class="img-fluid" alt="" />
				</div>
			</div>
		</div>
		</div>
		</div>
		<!-- //seccion temporadas -->

		<!-- formulario para jugadores -->
		<section class="text py-5" id="register">
			<div class="container py-md-5 py-3 text-center">
				<div class="row">
					<div class="col-12">
						<h2 class="heading-agileinfo text-center pb-4 text-white">Únete al equipo, <span>forma parte del
								huracán.</span></h2>
						<p>Completa el siguiente formulario indicando tu categoría y nos pondremos en contacto contigo.
							<br>¡Estamos deseando conocerte!.</p>
						<a href="categorias.php" class="btn mr-3"> Categorías</a>
						<a href="contact.php" class="btn"> Contacto </a>
					</div>
					<div class="col-md-6 padding">
						<form action="PHPMailer2/enviar-email.php" name="enviar" method="post">
							<h5 class="mb-3 text-azul">Hazte Hurricane</h5>
							<div class="form-style-w3ls">
								<input placeholder="Tu nombre" name="name" type="text" required="">
								<input placeholder="Tu email" name="email" type="email" required="">
								<input placeholder="Télefono de Contacto" name="telefono" type="text" required="">
								<select name="categorias" type="text">
									<option value="Senior">Senior</option>
									<option value="Femenino">Femenino</option>
									<option value="Junior">Junior</option>
									<option value="Flag">Flag</option>
								</select>
								<button Class="btn"> Enviar</button>
								<span class="text-white">Al registrarse, usted acepta nuestros<a href="privacidad.php">Terminos &
										Condiciones.</a></span>
							</div>
						</form>
					</div>
				</div>
			</div>
			
			
			<!-- //formulario para jugadores -->

	<!-- footer -->
	<?php
include "includes/footer.php"
?>
<!-- //footer -->


	<!-- js-scripts -->
	<?php
include "includes/script.php"
?>
<!-- //js-scripts -->


</body>

</html>
</html>