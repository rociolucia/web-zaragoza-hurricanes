<!DOCTYPE html>
<html lang="es">

<head>
	<title>Zaragoza Hurricanes – Fútbol Americano en Zaragoza</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta charset="utf-8" />
	<meta name="keywords"
		content="Zaragoza Hurricanes, Equipo de futbol americano de Zaragoza, football, futbol, gobierno de Aragon, masterd, zaragoza deporte, gran cafe zaragozano, Los Lions, calendario deportivo, facebook, instagram, twiter, youtube, club de football" />
	<!-- script/ carga de página -->
	<script>
		addEventListener("load", function () {
			setTimeout(hideURLbar, 0);
		}, false);

		function hideURLbar() {
			window.scrollTo(0, 1);
		}
	</script>
	<!-- //script/ carga de página -->

	<!-- estilos formulario -->
	<link rel="stylesheet" href="css/owl.carousel.css" type="text/css" media="all" />
	<link href="css/popuo-box.css" rel="stylesheet" type="text/css" media="all" />
	<link rel="stylesheet" href="css/flexslider.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="css/slider.css" type="text/css" media="all" />
	<!-- //estilos formulario -->
	<!-- estilos welcomepack -->
	<link rel="stylesheet" href="assets3/css/style-starter.css" type="text/css" media="all" />
    <link rel="stylesheet" href="assets3/fonts/fontawesome-webfont.svg" type="text/css" media="all" />
    <link rel="stylesheet" href="assets3/fonts/fontawesome-webfont.svg" type="text/css" media="all" />
	<link href="css/bootstrap.css" type="text/css" rel="stylesheet" media="all">
	<link href="css/style.css" type="text/css" rel="stylesheet" media="all">
	<link rel="stylesheet" href="css/style2.css" type="text/css" media="all" />
	<link href="css/style4.css" type="text/css" rel="stylesheet" media="all">
	<!-- //estilos welcomepack -->
	<!-- iconos -->
	<link rel="stylesheet" href="css/fontawesome-all.css">
	<link href="css/fontawesome-all.min.css" rel="stylesheet">
	<!-- //iconos -->
	<!-- tipografias -->
	<link href="//fonts.googleapis.com/css?family=Ubuntu:300,300i,400,400i,500,500i,700,700i" rel="stylesheet">
	<link
		href="//fonts.googleapis.com/css?family=Montserrat:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i&amp;subset=cyrillic,cyrillic-ext,latin-ext,vietnamese"
		rel="stylesheet">
	<link
		href="//fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i&amp;subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese"
		rel="stylesheet">
	<!-- //tipografias -->
</head>

<body>

<!-- menu -->
<?php
include "includes/menu.php"
?>
<!-- //menu -->

	<div class="inner-page-banner text-center">
		<div class="banner-dott3">
			<div class="container">
				<br><br><br><br>
				<h2 class="text-capitalize"></h2>
				<strong>
					<p><a href="index.php"></a></p>
				</strong>
			</div>
		</div>
	</div>
	<!-- //logo + menu -->
	<!--// header -->


	<!-- formulario welcmepack -->
	<section class="text py-5" id="register">
		<div class="container py-md-5 py-3 text-center">
			<div class="row">
				<div class="col-12">
					<h2 class="heading-agileinfo text-center pb-4 text-white">Consigue tu <span>Welcome Pack</span></h2>
					<p>Completa el siguiente formulario indicando tus datos y tu talla de camiseta. <br>¡Ven a disfrutar
						con nosotros del Football!.</p>
					<a href="categorias.php" class="btn mr-3"> Categorías</a>
					<a href="contact.php" class="btn"> Contacto </a>
				</div>
				<div class="col-md-6 padding">
					<form action="PHPMailer/enviar-email.php" name="enviar" method="post">
						<h5 class="mb-3 text-azul">Forma parte del huracán.</h5>
						<div class="form-style-w3ls">
							<input placeholder="Tu nombre" name="name" type="text" required="">
							<input placeholder="Tu email" name="email" type="email" required="">
							<input placeholder="Télefono de Contacto" name="telefono" type="text" required="">
							<select name="tallas" type="text">
								<option value="S">Talla S</option>
								<option value="M">Talla M</option>
								<option value="L">Talla L</option>
								<option value="XL">Talla XL</option>
							</select>
							<button Class="btn"> Enviar</button>
							<span class="text-white">Al registrarse, usted acepta nuestros<a href="privacidad.php">Terminos &
									Condiciones.</a></span>
								
						</div>
					</form>
				</div>
				
				
			</div>
		</div>
		
		<!-- //formulario welcmepack -->

		<!-- footer -->
	<?php
include "includes/footer.php"
?>
<!-- //footer -->


	<!-- js-scripts -->
	<?php
include "includes/script.php"
?>
<!-- //js-scripts -->


</body>

</html>